import { stringify } from 'qs'

export default function navigateTo(url, options) {
    // 页面栈只能由一个阅读界面 不能同时存在多个
    const pages = getCurrentPages()
    let inPages = false
    for (let i = pages.length - 1; i >= 0; i--) {
        const page = pages[i]
        if (page.route == url) {
            inPages = true
            page.onLoad(options)
            uni.navigateBack({
                delta: pages.length - 1 - i
            })
            break
        }
    }
    if (!inPages) {
        uni.navigateTo({
            url: '/' + url + '?' + stringify(options, { encode: false })
        })
    }
}