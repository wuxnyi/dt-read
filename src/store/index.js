import Vue from "vue";
import Vuex from "vuex";
import fetch from "../common/request.js";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    version: "2.0.0",
    isLogin: false,
    bookshelfs: [],
    openid: "",
    userInfo: {
      // 用户信息
      longitude: null, // 经度
      latitude: null, // 纬度
      locationCity: "" // 定位城市
    },
    config: {
      // 背景颜色
      bgColor: "#D5EED1",
      // 字体颜色
      color: "#282D29",
      // 是否夜间模式
      isNight: false,
      // 亮度
      brightness: 0,
      // 是否跟随系统亮度
      followSysBright: true,
      // 系统亮度
      sysBright: 0,
      // 字体大小
      fontSize: 40,
      // 行间距
      lineHeight: 56,
      // 常亮
      sleep: 5,
      // 翻页动画
      animate: "none"
    },

    mobile: uni.getStorageSync("thorui_mobile") || "echo.",
    memberId: uni.getStorageSync("memberId") || 0,
    appInfo: {
      // APP整体数据
      commonDataVersion: "0", // 公共数据的大版本号
      adVersion: "0", // 广告数据版本号
      serviceVersion: "0" // 服务数据版本号
    },
    adData: {
      homePageAdverts: [],
      carPageAdverts: [],
      servicePageAdverts: []
    },
    modulesData: {
      // 首页服务模块、产品模块和服务页服务模块数据
      serviceModules: [],
      productModules: [],
      serviceFuncVOList: []
    }
  },
  mutations: {
    LOGIN(state, payload) {
      if (payload) {
        state.openid = payload.openid;
        // state.mobile = payload.mobile;
        // state.memberId = payload.memberId;
      }
      state.isLogin = true;
    },
    logout(state) {
      state.mobile = "";
      state.memberId = 0;
      state.isLogin = false;
    },
    setOpenid(state, openid) {
      state.openid = openid;
    },
    // 设置用户信息
    setUserInfo(state, payload) {
      for (let i in payload) {
        for (let j in state.userInfo) {
          if (i === j) {
            state.userInfo[j] = payload[i];
          }
        }
      }
    },
    // 设置APP信息
    setAppInfo(state, payload) {
      for (let i in payload) {
        for (let j in state.appInfo) {
          if (i === j) {
            state.appInfo[j] = payload[i];
          }
        }
      }
    },
    // 设置配置信息
    setConfig(state, payload) {
      state.config = { ...state.config, ...payload };
    },
    // 设置书架信息
    setBookshelfs(state, payload) {
      state.bookshelfs = [...payload];
    },
    // 更新APP整体广告数据
    updateAdData(state, payload) {
      uni.setStorageSync(
        "carPageAdverts",
        JSON.stringify(payload.adData.carPageAdverts)
      );
      state.adData = payload.adData;
    },
    // 更新APP整体服务数据
    updateModulesData(state, payload) {
      state.modulesData = payload.modulesData;
    }
  },
  actions: {
    async login({ commit }) {
      const res = await wx.cloud.callFunction({
        name: "login"
      });
      const { openid, config } = res.result.data;
      commit("LOGIN", { openid });
      if (config) {
        commit("setConfig", config);
      }
    },
    // 添加书架
    addBookshelfs({ commit, state }, payload) {
      commit("setBookshelfs", [...state.bookshelfs, payload]);
    },
    // 查看公共数据版本是否更新
    // 返回有data数据时代表有更新，未返回data数据代表不需要更新
    // 返回9010错误时代表还未生成公共数据版本，需要先调用对应接口生成数据版本号
    async checkModuleUpdate({ commit, state }) {
      return new Promise((resolve, reject) => {
        fetch
          .request(
            "config/queryHasUpdates",
            {
              version: state.appInfo.commonDataVersion
            },
            "POST"
          )
          .then(res => {
            //console.log('各个模块数据数据' + JSON.stringify(res))
            if (res.code === 200) {
              if (!res.data) return;
              const data = res.data.versionData;
              let obj = {
                updateAd: false,
                updateService: false
              };
              commit("setAppInfo", {
                commonDataVersion: res.data.version
              });
              if (data.advertVersion !== state.appInfo.adVersion) {
                obj.updateAd = true;
              }
              if (data.serviceVersion !== state.appInfo.serviceVersion) {
                obj.updateService = true;
              }
              resolve(obj);
            } else {
              fetch.toast(res.message);
            }
          })
          .catch(e => { });
      });
    },
    // 查询广告（首页、车圈、服务）
    getAds({ dispatch, commit, state }) {
      fetch
        .request(
          "config/queryAdverts",
          {
            clientDictKey: 1,
            version: state.appInfo.adVersion
          },
          "POST"
        )
        .then(res => {
          if (res.code === 200) {
            commit("updateAdData", {
              adData: res.data
            });
            commit("setAppInfo", {
              adVersion: res.data.version
            });
          } else {
            fetch.toast(res.message);
          }
        })
        .catch(e => { });
    },
    // 获取首页服务模块、产品模块和服务页服务模块数据
    getServices({ commit, state }) {
      fetch
        .request(
          "serviceFunc/searchServiceFunc",
          {
            version: state.appInfo.serviceVersion
          },
          "POST"
        )
        .then(res => {
          // console.log('首页服务模块、产品模块和服务页服务模块数据' + JSON.stringify(res.data))
          if (res.code === 200) {
            commit("updateModulesData", {
              modulesData: res.data.data
            });
            commit("setAppInfo", {
              serviceVersion: res.data.version
            });
          } else {
            fetch.toast(res.message);
          }
        })
        .catch(e => { });
    }
  }
});

export default store;
